import { Component, OnInit,EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent implements OnInit {

  @Output()
  emisor = new EventEmitter<string>();

  resultado:number;
  numer1:number;
  numer2:number;
  muestra:boolean = false;
  error:string ="";

  constructor() { }

  ngOnInit() {
  }

sumar(){
  if(this.numer1==null || this.numer2==null){
    this.muestra=true;
    this.error="Debe llenar los numeros";
  }else{
  this.resultado = Number(this.numer1) +  Number(this.numer2);
  this.numer1 =Number(this.resultado);
  this.numer2 = null ;
  this.muestra=false;
  this.imprimir();
  }
}

restar(){
  if(this.numer1==null || this.numer2==null){
    this.muestra=true;
    this.error="Debe llenar los numeros";
  }else{
  this.resultado = Number(this.numer1) - Number(this.numer2);
  this.numer1 =Number(this.resultado);
  this.numer2 = null ;
  this.muestra=false;
  this.imprimir();
  }
}

multiplicar(){
  if(this.numer1==null || this.numer2==null){
    this.muestra=true;
    this.error="Debe llenar los numeros";
  }else{
  this.resultado = Number(this.numer1) *  Number(this.numer2);
  this.numer1 =Number(this.resultado);
  this.numer2 = null ;
  this.muestra=false;
  this.imprimir();
  }
}

dividir(){
  if(this.numer1==null || this.numer2==null){
    this.muestra=true;
    this.error="Debe llenar los numeros";
  }
  else if(Number(this.numer2)==0){
    this.muestra=true;
    this.error="no se puede dividir por 0";
  }else{
  this.resultado = Number(this.numer1) /  Number(this.numer2);
  this.numer1 =Number(this.resultado);
  this.numer2 = null ;
  this.muestra=false;
  this.imprimir();
}
}

clean(){
this.numer1=null;
this.resultado=null;
this.numer2=null;
this.muestra=false;
this.error="";
this.borrar();
}

imprimir(){
  this.emisor.emit("El ultimo resultado es: "+this.resultado);
}

borrar(){
  this.emisor.emit("");
}
}
